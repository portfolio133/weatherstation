#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include "DHT.h"
#define DHTPIN 2
#define DHTTYPE DHT11

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);
DHT dht(DHTPIN, DHTTYPE);
int h20 = 1;
void setup()
{
  //initialize H20 sensor
  pinMode(h20, INPUT);
  Serial.begin(9600);
	// initialize the LCD
	lcd.begin();
  Serial.begin(9600);
  //lcd.println(F("DHTxx test!"));
  dht.begin();

	// Turn on the blacklight and print a message.
	lcd.backlight();
	lcd.print("Ready!");
}

void loop()
{
	// Wait a few seconds between measurements.
  delay(1000);
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    lcd.clear();
    lcd.print(F("Failed to read!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);
    int i = analogRead(h20);
  if(i>400)
    {
      lcd.clear();
      lcd.print("Pada Deszcz!");
      lcd.setCursor(0,1);
      //lcd.print(i);
    }
  else
    {
      //lcd.print(F("Humidity: "));
      //lcd.setcursor(0,1);
      lcd.clear();
      lcd.print(F("T: "));
      lcd.print(t);
      lcd.print(F("*C "));
      lcd.setCursor(0,1);
      lcd.print(F("W: "));
      lcd.print(h);
      lcd.print("%");
    }

  //lcd.print(f);
  //lcd.print(F("°F  Heat index: "));
  //lcd.print(hic);
  //lcd.print(F("°C "));
  //lcd.print(hif);
  //lcd.println(F("°F"));
}
